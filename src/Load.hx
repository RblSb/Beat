package;

import kha.Assets;
import kha.Image;
import kha.Sound;

typedef Frame = {
	?w:Int,
	?x:Int,
	?y:Int,
	?rep:Int
}

typedef Anim = {
	name:String,
	x:Int,
	y:Int,
	frames:Array<Frame>
}

typedef Anims = {
	anims:Array<Anim>
}

class Load {

	public static var sound0:Sound;

	public static var char0_walk:Image;
	public static var char0_retreat:Image;
	public static var char0_stunned0:Image;
	public static var char0_stunned1:Image;
	public static var char0_attack0:Image;
	public static var char0_attack1:Image;
	public static var char0_attack2:Image;

	public function new() {}

	public function load():Void {
		sound0 = Assets.sounds.sound0;

		loadAnims();
	}

	public function loadAnims():Void {
		var json = Reflect.field(Assets.blobs, "char_json").toString();
		var r:Anims = haxe.Json.parse(json);

		for (i in 0...r.anims.length) {
			var anim = r.anims[i];
			var anim_image = Reflect.field(Assets.images, "char_" + anim.name);
			//trace(anim_image.width, anim_image.height);

			var frames_img = [];
			var xx = 0;

			for (j in 0...anim.frames.length) {
				var frame = anim.frames[j];
				var width = 64;//frame.w;

				if (frame.rep != null) {
					var rep = frame.rep;
					frames_img.push(frames_img[rep]);
				} else {
					anim.x = 0;frame.x = 0;//if (frame.x == null) frame.x = 0;
					anim.y = 0;frame.y = 0;//if (frame.y == null) frame.y = 0;
					var frame_image = Image.createRenderTarget(64, 64, 0xFFFFFFFF); //128
					var g = frame_image.g2;

					g.begin(false);
					g.color = 0x300000FF;
					//g.fillRect(0, 0, 32, 32);
					g.color = 0xFFFFFFFF;
					g.drawScaledSubImage(
						anim_image, xx, 0, 64, 64,
						anim.x + frame.x, anim.y + frame.y, 64, 64//128
					);
					g.end();

					frames_img.push(frame_image);
					xx += width;
				}
			}

			var image = Image.createRenderTarget(frames_img.length * 64, 64);//128
			image.g2.begin(false);
			for (i in 0...frames_img.length) image.g2.drawImage(frames_img[i], i * 64, 0);//128
			image.g2.end();

			switch(anim.name) {
			case "stand": char0_walk = image;
			case "retreat": char0_retreat = image;
			case "stunned0": char0_stunned0 = image;
			case "stunned1": char0_stunned1 = image;
			case "attack0": char0_attack0 = image;
			case "attack1": char0_attack1 = image;
			case "attack2": char0_attack2 = image;
			}
		}
	}
}