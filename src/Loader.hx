package;

import kha.System;
import kha.Framebuffer;
import kha.Assets;

class Loader {

	public function new() {}

	public function init():Void {
		System.notifyOnFrames(onRender);
		Assets.loadEverything(loadComplete);
	}

	function loadComplete():Void {
		System.removeFramesListener(onRender);
		var game = new Game();
		game.init();
	}

	function onRender(fbs:Array<Framebuffer>):Void {
		var g = fbs[0].g2;
		g.begin();
		var w = System.windowWidth() * Assets.progress;
		var halfH = 10;
		var y = System.windowHeight() / 2 - halfH;
		g.fillRect(0, y, w, halfH * 2);
		g.end();
	}
}
