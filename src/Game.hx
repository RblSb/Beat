package;

import kha.System;
import kha.Canvas;
import kha.graphics2.Graphics;
import kha.math.FastMatrix3;
import kha.Assets;
import kha.input.KeyCode;
import kha.Font;
import kha.audio1.AudioChannel;
import kha.audio1.Audio;
import Human.States;
import khm.Screen;
import khm.Screen.Pointer;

class Game extends Screen {

	//var backbuffer:Image;
	var font:Font;
	var load:Load;
	var audioChannel:AudioChannel;
	var lvl:Lvl;
	var human:Human;
	var enem:Array<Human> = [];

	//public function new() {}

	public function init():Void {
		show();
		setScale(2);
		load = new Load();
		load.load();
		font = Assets.fonts.fixedsys;
		audioChannel = Audio.play(Load.sound0);
		audioChannel.volume = 0.2;
		audioChannel.stop();

		lvl = new Lvl();
		lvl.init();
		human = new Human();
		human.init(0, 60, 0, 0);
		human.id = 0;
		human.skinColor = 0xFFFFFFFF;
		human.health = 4;
		human.dir = 1;
		for (i in 0...0) {
			enem[i] = new Human();
			enem[i].init(0, 40 + i * 8, 35, 0);
			enem[i].id = i + 1;
		}
	}

	function collideRects(att:Human, dfn:Human):Bool {
		if (att.id == dfn.id || dfn.z - att.z > 3 || att.z - dfn.z > 3) return false;

		var isCollide = false;
		var r1 = att.attackRect;
		var r2 = dfn.bodyRect;

		if (r1.x + r1.w >= r2.x &&
			r1.x <= r2.x + r2.w &&
			r1.y + r1.w >= r2.y &&
			r1.y <= r2.y + r2.h)
			isCollide = true;
		return isCollide;
	}

	inline function execAttack(att:Human) {
		if (att.state == States.attack &&
		att.frame_n >= 2 && att.frame_n < 2.1) {
			for (dfn in enem) {
				if (collideRects(att, dfn) == true) {
					execPunch(att, dfn);
					dfn.changeState(States.stunned);
					dfn.dir = -1 * att.dir;
					att.prevSuccess = true;
				}
			}

			if (att.id > 0) {
				if (collideRects(att, human)) {
					execPunch(att, human);
					human.changeState(States.stunned);
					human.dir = -1 * att.dir;
					att.prevSuccess = true;
				}
			}
		}
	}

	inline function execPunch(att:Human, dfn:Human) {
		//trace("punch");
		audioChannel.stop();
		audioChannel.play();
		att.g = 0;
		dfn.g = 0;
		dfn.hspeedLock = 0;
		switch (att.attackMode) {
		case 0:
			att.hspeed = 2 * att.dir;
			dfn.hspeed = 2 * att.dir;
			dfn.vspeed = -1;
		case 1:
			att.hspeed = 2 * att.dir;
			dfn.hspeed = 2 * att.dir;
			dfn.vspeed = -1;
		case 2:
			att.vspeed = -1.6;
			dfn.vspeed = -2.6;
		}
		dfn.attackMode = att.attackMode;
		dfn.health -= 0.334;
	}

	override function onUpdate():Void {
		//grid = (input[32] ? 16 : 32);
		if (keys[KeyCode.Shift] == true) {
			if (keys[KeyCode.A] == true) {
				keys[KeyCode.A] = false;
				human.frame_n--;
			}
			if (keys[KeyCode.D]) {
				keys[KeyCode.D] = false;
				human.frame_n++;
			}
			return;
		}

		if (human.onLand == true) {
			if (keys[KeyCode.A] == true) human.walkSpeed = -1;
			if (keys[KeyCode.D] == true) human.walkSpeed = 1;
			if (keys[KeyCode.W] == true) human.z -= 0.5;
			if (human.z < 0) human.z = 0;
			if (keys[KeyCode.S] == true) human.z += 0.5;
			if (human.z > 58) human.z = 58;

			if (keys[KeyCode.O] == true) {
				keys[KeyCode.O] = false;
				if (human.state == States.walk) {
					human.nextPunch = 1;
				}
			}

			if (keys[KeyCode.P] == true) {
				keys[KeyCode.P] = false;
				if (human.state == States.walk ||
				human.state == States.retreat ||
				human.state == States.attack) {
					human.nextPunch = 2;
				}
			}
		}

		human.comboTick();
		human.update();
		execAttack(human);

		for (e in enem) {
			e.nextPunch = 2;
			e.comboTick();
			e.update();
			execAttack(e);
		}

		/*for (i in 0...enem.length) {
			if (enem[i].onLand == true) {
				if (human.bodyRect.x + 10 < enem[i].bodyRect.x) {
					enem[i].walkSpeed = -0.2;
				} else if (human.bodyRect.x - 10 > enem[i].bodyRect.x) {
					enem[i].walkSpeed = 0.2;
				} else if (enem[i].state == States.walk || enem[i].state == States.attack) {
					//enem[i].nextPunch = 2;
				}
			}
			enem[i].comboTick();
			enem[i].update();

			if (enem[i].state == States.attack &&
			collideRects(enem[i].attackRect, human.bodyRect) &&
			enem[i].frame_n >= 2 && enem[i].frame_n < 2.1) {
				execAttack(enem[i], human);
				human.changeState(States.stunned);
				human.dir = -1 * enem[i].dir;
				enem[i].prevSuccess = true;
			}
		}*/

		lvl.updateView(human.bodyRect.x);
	}

	function drawHealth(g:Graphics, human:Human, x:Int, y:Int):Void {
		/*var d0 = Math.floor(human.health);
		var i = 0;
		for (i in 0...d0) {
			g.color = 0xFF00FF00;
			g.fillRect(x + i * 12, y, 11, 5);
			g.color = 0xFF000000;
			g.drawRect(x + 1 + i * 12, y + 1, 10, 5);
		}
		var d1 = human.health - d0;
		if (d0 >= 0 && d1 > 0) {
			//trace(d0);
			g.color = 0xFF00FF00;
			g.fillRect(x + d0 * 12, y, 11 * d1, 5);
			g.color = 0xFF000000;
			g.drawRect(x + 1 + d0 * 12, y + 1, 10 * d1, 5);
		}*/
		g.color = 0xFFFF0000;
	}

	override function onRender(frame:Canvas):Void {
		//var g = backbuffer.g2;
		var g = frame.g2;
		g.begin();
		g.font = font;
		g.fontSize = 16;

		g.color = 0xFF4FBFE9;//g.color = 0xFFD96784;
		g.fillRect(0, 0, Screen.w, Screen.h);
		g.color = 0xFFFFFFFF;

		lvl.render(g);
		for (e in enem) e.render(g);
		human.render(g);
		//g.drawString("" + Math.floor(human.frame_n), 0, screen_h - 16);
		//g.drawString("" + Screen.w + "x" + Screen.h, 0, 0);
		//g.drawString("" + Lvl.vx + "/" + Lvl.maxVx, 0, 16);
		//drawHealth(g, human, 2, 2);
		//drawHealth(g, enem[0], 2, 12);

		//g.fillRect(pointers[0].x, pointers[0].y, 5, 5);
		g.end();
	}

	/*override function onMouseDown(p:Pointer):Void {
		trace(p);
	}*/
}
